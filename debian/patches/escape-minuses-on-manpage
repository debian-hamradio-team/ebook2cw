Description: Escaping minus signs on man page
Author: Christoph Feenders <debian@echti.de>
Last-update: 2022-06-05

Escaping hypthons "-" --> "\-" on manual page to explicitly mark them as minus
signs. Cures lintian tag "hyphen-used-as-minus-sign".

--- a/ebook2cw.1
+++ b/ebook2cw.1
@@ -12,105 +12,105 @@
 A number of CW and audio parameters can be changed from their default values, by
 command line switches or a config file (see below). These are (default values in brackets):
 
-.B -w wpm 
-- CW speed in words per minute [25]
+.B \-w wpm
+\- CW speed in words per minute [25]
 
-.B -e wpm 
-- Effective CW speed. If set, the spaces are sent at this speed instead of the
-character speed set by -w ("Farnsworth"). 
+.B \-e wpm
+\- Effective CW speed. If set, the spaces are sent at this speed instead of the
+character speed set by \-w ("Farnsworth").
 
-.B -W x
-- Extra Word spacing. Similar to -e, but only affects the inter-word spacing,
-not the inter-character spacing. 
+.B \-W x
+\- Extra Word spacing. Similar to \-e, but only affects the inter\-word spacing,
+not the inter\-character spacing.
 
-.B -f freq 
-- audio frequency in Hz [600]
+.B \-f freq
+\- audio frequency in Hz [600]
 
-.B -T SINE|0|SAWTOOTH|1|SQUARE|2 
-- set waveform to sine, sawtooth, squarewave. [sine]
+.B \-T SINE|0|SAWTOOTH|1|SQUARE|2
+\- set waveform to sine, sawtooth, squarewave. [sine]
 
-.B -Q minutes
-- Increase CW speed (QRQ) by 1 WpM in intervals of `minutes'. Speed will be 
+.B \-Q minutes
+\- Increase CW speed (QRQ) by 1 WpM in intervals of `minutes'. Speed will be
 reset to the initial value at the start of each chapter. [0]
 
-.B -n
-- Disables resetting the speed when using the -Q option.
+.B \-n
+\- Disables resetting the speed when using the \-Q option.
 
-.B -p
-- Disables the paragraph separator (<BT>)
+.B \-p
+\- Disables the paragraph separator (<BT>)
 
-.B -R risetime 
-- risetime, in samples [50]
+.B \-R risetime
+\- risetime, in samples [50]
 
-.B -F falltime 
-- falltime, samples [50]
+.B \-F falltime
+\- falltime, samples [50]
 
-.B -O
-- Use OGG/Vorbis encoder instead of MP3 if compiled with OGG support
+.B \-O
+\- Use OGG/Vorbis encoder instead of MP3 if compiled with OGG support
 
-.B -X
-- Do not encode, do not generate output files
+.B \-X
+\- Do not encode, do not generate output files
 
-.B -s samplerate 
-- samplerate for the OGG/MP3 file [11025]
+.B \-s samplerate
+\- samplerate for the OGG/MP3 file [11025]
 
-.B -b bitrate 
-- MP3 bitrate, kbps [16]
+.B \-b bitrate
+\- MP3 bitrate, kbps [16]
 
-.B -q quality 
-- MP3 quality, 1 (best) to 9 (worst). CW still sounds very good with the worst quality, encoding time is greatly reduced. [5]
+.B \-q quality
+\- MP3 quality, 1 (best) to 9 (worst). CW still sounds very good with the worst quality, encoding time is greatly reduced. [5]
 
-.B -c chapter separator 
-- Split chapters at this string [CHAPTER]. If empty or starts with a dash,
+.B \-c chapter separator
+\- Split chapters at this string [CHAPTER]. If empty or starts with a dash,
   chapters will not be split and the output files will not be numbered.
 
-.B -d duration
-- Splits output files after "duration" seconds; finishes the current sentence.
+.B \-d duration
+\- Splits output files after "duration" seconds; finishes the current sentence.
 
-.B -l wordlimit
-- Splits output files after "wordlimit" words; finished the current sentence.
+.B \-l wordlimit
+\- Splits output files after "wordlimit" words; finished the current sentence.
 
-.B -o outfile-name 
-- Output filename (chapter number and .mp3/.ogg will be appended) [Chapter]
+.B \-o outfile-name
+\- Output filename (chapter number and .mp3/.ogg will be appended) [Chapter]
 
-.B -a author
-- Author for the ID3 tag. Use quotes for strings with spaces (e.g. "JW Goethe")
+.B \-a author
+\- Author for the ID3 tag. Use quotes for strings with spaces (e.g. "JW Goethe")
 
-.B -t title
-- Title for the ID3 tag. Use quotes for strings with spaces (e.g. "Faust II")
+.B \-t title
+\- Title for the ID3 tag. Use quotes for strings with spaces (e.g. "Faust II")
 
-.B -k comment
-- Comment for the ID3 tag. Use quotes for strings with spaces.
+.B \-k comment
+\- Comment for the ID3 tag. Use quotes for strings with spaces.
 
-.B -y year
-- Year for the ID3 tag.
+.B \-y year
+\- Year for the ID3 tag.
 
-.B -u 
-- Switches input encoding format to UTF-8. Currently supported alphabets
-include Latin, Greek, Hebrew, Arabic and Cyrillic. Default is ISO 8859-1.
+.B \-u
+\- Switches input encoding format to UTF\-8. Currently supported alphabets
+include Latin, Greek, Hebrew, Arabic and Cyrillic. Default is ISO 8859\-1.
 
-.B -E file
-- Loads configuration from `file`.
+.B \-E file
+\- Loads configuration from `file`.
 
-.B -g file
-- Guesses the encoding of `file` (ISO 8859-1 / ASCII or UTF-8).
+.B \-g file
+\- Guesses the encoding of `file` (ISO 8859\-1 / ASCII or UTF\-8).
 
-.B -S [ISO|UTF]
-- Shows a table of all available morse symbols for the ISO 8859-1 and UTF-8
+.B \-S [ISO|UTF]
+\- Shows a table of all available morse symbols for the ISO 8859\-1 and UTF\-8
 character sets. Output in HTML format.
 
-.B -N snr
-- When this option is used, a noise background is added to the file and the CW
+.B \-N snr
+\- When this option is used, a noise background is added to the file and the CW
 signal is scaled down to achieve a SNR (Signal to Noise ratio) of "snr" dB. 
-Possible range of SNR: -10db to 10dB. Make sure to enclose the value in 
-quotation marks if it's negative (i.e. -N "-3").
+Possible range of SNR: \-10db to 10dB. Make sure to enclose the value in
+quotation marks if it's negative (i.e. \-N "\-3").
 
-.B -B bandwidth in Hz
-- Sets the filter bandwidth if the -N / SNR option is used. Available filters
+.B \-B bandwidth in Hz
+\- Sets the filter bandwidth if the \-N / SNR option is used. Available filters
 are 100Hz, 500Hz, 1kHz and 2.1kHz. 
 
-.B -C frequency in Hz
-- Sets the center frequency of the filter if the -N / SNR option is used. This
+.B \-C frequency in Hz
+\- Sets the center frequency of the filter if the \-N / SNR option is used. This
 should be set to the frequency of the Morse signal; currently implemented
 center frequencies: 800Hz
 
@@ -133,8 +133,8 @@
 can be changed by command line parameters can be set. Any settings made in the
 config file can be overridden by command line arguments.
 
-Additionally, two `map` files can be set in the config file, for ISO8859-1 and
-UTF-8. You can map characters in those files to a string, which may be useful
+Additionally, two `map` files can be set in the config file, for ISO8859\-1 and
+UTF\-8. You can map characters in those files to a string, which may be useful
 to replace characters like the exclamation mark (!) to a period (.), which is
 more common in CW.
 
